package com.epam.view;

import com.epam.Application;
import com.epam.statepattern.Tasks;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - To do"));
        menu.put("2", ("2 - In progress"));
        menu.put("3", ("3 - Ready for deploy"));
        menu.put("4", ("4 - QA"));
        menu.put("5", ("5 - PM approving"));
        menu.put("6", ("6 - Done"));
        menu.put("7", ("7 - Quantity of even numbers"));
        menu.put("8", ("8 - Show all even numbers"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("Q", this::pressButton9);


    }
Tasks taskSart=new Tasks();
    private void pressButton1()  {
    taskSart.toDo();
    }

    private void pressButton2()  {
    taskSart.inProgress();
    }

    private void pressButton3()  {
taskSart.readyFordeploy();
    }

    private void pressButton4()  {
taskSart.qualityAssurance();
    }

    private void pressButton5()  {
taskSart.approving();
    }

    private void pressButton6()  {
taskSart.done();
    }

    private void pressButton7()  {
logger1.info(Tasks.countEven);
    }

    private void pressButton8()  {
logger1.info(taskSart.getNumbers());
    }

    private void pressButton9()  {
        logger1.info("Bye-Bye");
    }



    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nKanban:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


