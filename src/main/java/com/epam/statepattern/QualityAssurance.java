package com.epam.statepattern;

/**
 * Implementation of approving step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class QualityAssurance implements State {


    @Override
    public void toDo(Tasks tasks) {

        tasks.setState(new ToDo());
        logger1.info("Application has quality issues");
        tasks.getNumbers().clear();
        Tasks.countEven = 0;
    }

    @Override
    public void approving(Tasks tasks) {

        tasks.setState(new Approving());
        logger1.info("1 - Return to the beginning in case of some errors");
        logger1.info("6 - Approved, quality issues ignored");
    }
}
