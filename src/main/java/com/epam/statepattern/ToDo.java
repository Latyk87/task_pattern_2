package com.epam.statepattern;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementation of to do step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class ToDo implements State {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @Override
    public void toDo(Tasks tasks) {
        logger1.info("Create ArrayList with even numbers [0:100], " +
                "and count the quantity of all even numbers");
    }

    @Override
    public void inProgress(Tasks tasks) {
        tasks.setState(new InProgress());
        logger1.info("Development started");
    }
}
