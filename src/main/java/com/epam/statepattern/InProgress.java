package com.epam.statepattern;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementation of in progress step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class InProgress implements State {
    Logger logger1 = LogManager.getLogger(Application.class);

    @Override
    public void readyFordeploy(Tasks tasks) {
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                tasks.getNumbers().add(i);
            }
        }
        for (Integer i : tasks.getNumbers()) {
            if (i % 2 == 0) {
                Tasks.countEven++;
            }
        }

        tasks.setState(new ReadyForDeploy());
        logger1.info("ArrayList filed by a defined period and even numbers counted");
    }
}