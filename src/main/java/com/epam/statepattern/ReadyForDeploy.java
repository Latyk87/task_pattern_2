package com.epam.statepattern;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementation of ready for deploy step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class ReadyForDeploy implements State {
    Logger logger1 = LogManager.getLogger(Application.class);

    @Override
    public void qualityAssurance(Tasks tasks) {
        if (Tasks.countEven == 50) {
            logger1.info("Quality test passed, quality issues sent to PM");
        }

        tasks.setState(new QualityAssurance());
        logger1.info("1 - You can return to the beginning if you has some doubts");
    }
}
