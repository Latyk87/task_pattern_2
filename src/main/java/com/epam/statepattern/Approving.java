package com.epam.statepattern;

/**
 * Implementation of approving step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class Approving implements State {

    @Override
    public void done(Tasks tasks) {
        tasks.setState(new Done());
        logger1.info("Sent to production");
    }
}
