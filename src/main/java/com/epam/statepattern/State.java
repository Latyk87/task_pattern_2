package com.epam.statepattern;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Interface with all steps in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public interface State {
    Logger logger1 = LogManager.getLogger(Application.class);

    default void toDo(Tasks tasks) {
        logger1.info("To do is not allowed");
    }

    default void inProgress(Tasks tasks) {
        logger1.info("In progress is not allowed");
    }

    default void readyFordeploy(Tasks tasks) {
        logger1.info("Ready for deploy is not allowed");
    }

    default void qualityAssurance(Tasks tasks) {
        logger1.info("QA is not allowed");
    }

    default void approving(Tasks tasks) {
        logger1.info("Approving is not allowed");
    }

    default void done(Tasks tasks) {
        logger1.info("Done is not allowed");
    }

}
