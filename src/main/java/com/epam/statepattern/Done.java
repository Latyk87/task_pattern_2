package com.epam.statepattern;

/**
 * Implementation of done step in Kanban process.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class Done implements State {
}
