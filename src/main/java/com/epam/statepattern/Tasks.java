package com.epam.statepattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Class describes tasks for Kanban.
 * Created by Borys Latyk on 21/12/2019.
 *
 * @version 2.1
 * @since 21.12.2019
 */
public class Tasks {
    private State state;
    public static int countEven = 0;
    private List<Integer> numbers = new ArrayList<>();

    public Tasks() {
        state = new ToDo();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void toDo() {
        state.toDo(this);
    }

    public void inProgress() {
        state.inProgress(this);
    }

    public void readyFordeploy() {
        state.readyFordeploy(this);
    }

    public void qualityAssurance() {
        state.qualityAssurance(this);
    }

    public void approving() {
        state.approving(this);
    }

    public void done() {
        state.done(this);
    }

    public List<Integer> getNumbers() {
        return numbers;
    }


}
